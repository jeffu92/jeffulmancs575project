angular.module('starter.services', [])

.factory('Socket', function ($rootScope) {
    var socket = io('http://ec2-52-34-11-38.us-west-2.compute.amazonaws.com:8888');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        },
        removeAllListeners: function () {
            socket.removeAllListeners();
        },
        getInspections: function() {
            socket.emit("getInspections");
        }
    }
});

