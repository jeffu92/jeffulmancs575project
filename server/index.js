var express = require("express");
var app = express();
app.use(express.static(__dirname + '/public'));
var port = 8888;

// start server
var server = app.listen(port);

// socket.io listens to the server
var io = require('socket.io').listen(server);

// DATABASE
// mongoose allows us easy to understand access to a mongoDB
var mongoose = require('mongoose');
// database is small for project, normally on different server with more memory
var url = 'mongodb://localhost/cs575';
mongoose.connect(url, function(error) {
        if (error)
                console.log(error);
});

// Schema defines the structure of the documents in a collection
var Schema = mongoose.Schema;
var InspectionsSchema = new Schema({
        name: String,
        defects: [{
                inspector: String,
                defectType: String,
                defectIcon: String,
                description: String,
                timestamp: Date
        }]
});

// Model allows us to create documents that have the Schemas definitions
// 'Inspection' correlates to the 'inspections' collection
var Inspections = mongoose.model('Inspection', InspectionsSchema);

// database functions
// ===================================================================

var onDeleteDefect = function(data) {
        // create Promise object that saves the defect to the specified inspection
        var deleteDefect = new Promise(function(resolve, reject) {
                // find inspection with inspectionId, and add the defect
                Inspections.findById(data.inspectionId, function(err, insp) {
                        if (err) reject("Error Finding Inspection: " + err);
                        else if (!insp) reject("Inspection Not Returned");
                        else {
                                // save defect to database
                                insp.defects.id(data.defectId).remove();
                                insp.save(function(err) {
                                        if (err) reject("Error Deleting Defect: " + err);
                                        else resolve("Defect Deleted");
                                });
                        }
                });
        });

        // call the Promise, if success send defect to all clients
        createDefect.then(function(result) {
                console.log(result);
                io.sockets.emit("defectCreated");
        }, function(err) {
                console.log(err);
        });
}

// create an inspection with no defects
var onCreateInspection = function(data) {
        // create inspection object
        var inspection = {
                name: data.inspectionName,
                defects: []
        }

        // create Promise object that saves the inspection to the database
        var createInspection = new Promise(function(resolve, reject) {
                Inspections.create(inspection, function(err) {
                        if (err) reject("Error Creating Inspection: " + err);
                        else resolve("Inspection Created");
                });
        });

        // call the Promise, if success send inspection to all clients
        createInspection.then(function(result) {
                console.log(result);
                io.sockets.emit("inspectionCreated");
        }, function(err) {
                console.log(err);
        });
}

// socket.io functions
// ==========================================================================

var onConnect = function(socket) {
        console.log("Client " + socket.id + " Connected");
        socket.on("getInspections", function() {
                Inspections.find({}, function(err, inspections) {
                        if (err) {
                                console.log("There was an error: " + err);
                        }
                        else io.to(socket.id).emit("inspectionsRetrieved", inspections);
                });
        });
        socket.on("deleteDefect", onDeleteDefect);
        socket.on("createDefect", onCreateDefect);
        socket.on("createInspection", onCreateInspection);

        socket.on("disconnect", function() {
                console.log("Client " + socket.id + " Disconnected");
        });
}

io.on("connection", onConnect);

console.log("Listening on port " + port);
