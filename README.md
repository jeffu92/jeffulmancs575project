### How do I get set up? ###

Make sure git is installed on your system. If you are unsure if you have git installed, please visit http://git-scm.com/.

Make sure Node.js is installed on your system. If you are unsure if you have Node.js installed, please visit https://nodejs.org/en/.

1. Start Git BASH (installed with git).
2. In Git BASH, navigate to where you would like your local repository to be stored.
3. All following commands should be executed in Git BASH. Type in the following commands sequentially.

        mkdir JeffUlmanCS575Project
        cd JeffUlmanCS575Project
        git init
        git clone https://jeffu92@bitbucket.org/jeffu92/jeffulmancs575project.git
        cd jeffulmancs575project

4. Install Ionic and its dependancies. "sudo" these if on Unix-based OS.

        npm install -g cordova
        npm install -g ionic


5. Run the Ionic project. This command hosts a development server on your machine temporarily. Choose to host on localhost, if given the option.

        ionic serve

At this point, you should see the project running in your default web browser. If you want to run this project in the future, navigate to this same folder and run "ionic serve".

### Who do I talk to? ###

* Jeffrey Ulman
* jeffu92@gmail.com