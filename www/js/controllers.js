var app = angular.module("starter.controllers", [])

app.controller("LoginCtrl", function ($scope, $rootScope, $state, Socket) {
    $rootScope.data = {
        username: ""
    };

    $scope.login = function () {
        $state.go("defects");
    }
});

app.controller("DefectsCtrl", function ($scope, $rootScope, $ionicModal, $ionicScrollDelegate, Socket) {

    // handlers for recieving messages
    //====================================================================
    Socket.on("inspectionsRetrieved", function (inspections) {
        var promise = new Promise(function(resolve, reject) {
            // if the returned inspections are a truthy value
            if (inspections) {
                $scope.inspections = inspections;
                // if the number of returned inspections is at least 1
                if (inspections.length > 0) {
                    // if an inspection has not been previously chosen
                    if (!$scope.data.inspectionChoice)
                        $scope.data.inspectionChoice = $scope.inspections[0]; // choose the first inspection by default
                    else
                        $scope.data.inspectionChoice = $scope.inspections[indexOfInspection($scope.data.inspectionChoice)];
                    resolve("Inspections Set");
                }
            }
            else reject("Inspection Not Returned");
        });

        promise.then(function (result) {
            $ionicScrollDelegate.resize();
        }, function (err) {
            // nothing
        });
    });

    var indexOfInspection = function(inspection) {
        for (i = 0; i < $scope.inspections.length; i++) {
            if ($scope.data.inspectionChoice._id === $scope.inspections[i]._id) {
                return i;
            }
        }
    }

    Socket.on("defectCreated", function () {
        Socket.getInspections();
    });

    Socket.on("defectDeleted", function () {
        Socket.getInspections();
    });

    Socket.on("inspectionCreated", function () {
        Socket.getInspections();
    });

    // methods for sending messages
    //==========================================================================
    $scope.createDefect = function () {
        if ($scope.data.inspectionChoice) {
            var data = {
                inspectionId: $scope.data.inspectionChoice._id,
                inspectorName: $rootScope.data.username,
                defectType: $scope.data.defectTypeChoice,
                defectIcon: findDefectTypeIconWithName($scope.data.defectTypeChoice),
                defectDescription: $scope.data.defectDescription
            }
            Socket.emit("createDefect", data);
        }
    }

    $scope.deleteDefect = function (defectId) {
        var data = {
            inspectionId: $scope.data.inspectionChoice._id,
            defectId: defectId
        }
        if ($scope.data.inspectionChoice) {
            Socket.emit("deleteDefect", data);
        }
    }

    $scope.createInspection = function (inspectionName) {
        var data = {
            inspectionName: inspectionName
        }
        Socket.emit("createInspection", data);
    }

    // misc
    //========================================================================

    var findDefectTypeIconWithName = function (defectType) {
        for (i = 0; i < $scope.data.defectTypes.length; i++) {
            if ($scope.data.defectTypes[i].name === defectType) return $scope.data.defectTypes[i].icon;
        }
    }

    var formatDate = function (timestamp) {
        var date = new Date(timestamp);
        return date;
    }

    // init
    //==========================================================================
    $scope.inspections = [];
    $scope.data = {
        inspectionChoice: null, // stores the inspection data that has been chosen by the user
        defectDescription: "",
        defectTypes: [
            {
                name: "Chemical",
                icon: "ion-erlenmeyer-flask"
            }, {
                name: "Electrical",
                icon: "ion-flash"
            }, {
                name: "Mechanical",
                icon: "ion-wrench"
            }
        ],
        defectTypeChoice: "Chemical"
    }

    Socket.getInspections();

    // Setting up the modals on the defects screen
    // add-defect-modal is modal1
    // choose-inspection-modal is modal2
    //===============================================================================
    $ionicModal.fromTemplateUrl("add-defect-modal.html", {
        id: 1,
        scope: $scope,
        animation: "slide-in-up"
    }).then(function (modal) {
        $scope.modal1 = modal;
    });

    $ionicModal.fromTemplateUrl("choose-inspection-modal.html", {
        id: 2,
        scope: $scope,
        animation: "slide-in-up"
    }).then(function (modal) {
        $scope.modal2 = modal;
    });

    $scope.openModal = function (index) {
        if (index == 1)
            $scope.modal1.show();
        else if (index == 2)
            $scope.modal2.show();

    };
    $scope.closeModal = function (index) {
        if (index == 1) {
            $scope.modal1.hide();
        }
        else if (index == 2) {
            $scope.modal2.hide();
        }
            
    };
    $scope.$on("$destroy", function () {
        $scope.modal1.remove();
        $scope.modal2.remove();
        Socket.removeAllListeners();
    });
});
